<?php
/**
 * @author   	cg@089webdesign.de
 */
 

defined('_JEXEC') or die;
//include system
include_once(JPATH_ROOT . "/templates/" . $this->template . '/lib/system.php');
//include template Functions CG
include_once(JPATH_ROOT . "/templates/" . $this->template . '/template_functions.php');
//
?>
<!DOCTYPE html>
<html lang="<?=$this->language?>" <?php //amp ?> >
<head>
	<?php 
	// including head
	include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/head.php');
	?>
	
</head>

<body id="body" class="site <?php echo $option
	. ' view-' . $view
	. ($layout ? ' layout-' . $layout : ' no-layout')
	. ($task ? ' task-' . $task : ' no-task')
	. ($itemid ? ' itemid-' . $itemid : '')
	. $body_class
	. print (!$detect->isMobile()) ? " desktop " : " mobile ";
?>">

	<!-- Body -->
		<div class="fullwidth site_wrapper">
			<?php			
			
			// including header
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/header.php');			

			// including header
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/menu.php');

			// including breadcrumb
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/breadcrumbs.php');				
					
			// including top
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/top.php');	
									
			// including content
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/content.php');	
			
			// including bottom
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/bottom.php');	
			
			// including bottom-2
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/bottom2.php');	

			// including footer
			include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/footer.php');				
			
			?>					
			
		</div>
	
	
	<jdoc:include type="modules" name="debug" style="none" />

	<script src="/templates/089-standard/js/jquery.lazyloadxt.extra.min.js" type="text/javascript" defer >
			jQuery(document).ready(function() {
				<?php if ($detect->isMobile()) : ?> //mobile

						jQuery.extend(jQuery.lazyLoadXT, { // lazyLoad
							  edgeY:  100,
							  srcAttr: 'data-src'
							});

				// submenue
				jQuery('.nav').find('.nav-child').css("display", "none");
				
				jQuery('#mobileToggle').click(function() {
					jQuery('.nav-child').slideToggle();
				});

						<?php else : ?> //desktop
							jQuery.extend(jQuery.lazyLoadXT, {
								  edgeY:  50,
							  	srcAttr: 'data-src'
							});    

				<?php endif;  ?>

		});				
	</script>
	<script type="text/javascript">

	jQuery(function() { //CG: oberes script ist "defer" deswegen hier extra deklarieren
		var deCookie = 'Diese Webseite verwendet Cookies. Wenn Sie diese Webseite nutzen, akzeptieren Sie die Verwendung von Cookies. Weitere Informationen erhalten Sie in unserer <a href="/datenschutz.html" id="cookiesck_readmore">Datenschutzerklärung</a> und <a class="cookieImp" href="/impressum.html"> Impressum</a>';
		var enCookie = 'We use cookies to improve the features and content offered to you. By continuing to use our site, you agree to this. Further information, please see under <a href="privacy-policy.html" id="cookiesck_readmore">Privacy Policy</a> and <a href="imprint.html" class="cookieImp">Imprint</a>.'
		var siteLang = jQuery('html').attr('lang');
		    //alert(theLanguage);
			(siteLang == "de-de") ? jQuery('.cookiesck_inner').append(deCookie) : jQuery('.cookiesck_inner').append(enCookie);
	});
	</script>

</body>
</html>
