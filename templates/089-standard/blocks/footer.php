<?php
/**
 * @author   	089webdesgin.de
 * @copyright   Copyright (C) 2015 089webdesgin.de. All rights reserved.
 * @URL 		https://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<footer class="footer" role="contentinfo">
	<div class="footer-wrap">				
		<div class="row-fluid">								
			<?php if ($this->countModules('footnav')) : ?>
			<div class="span8 footnav">
				<div class="module_footer position_footnav">
					<jdoc:include type="modules" name="footnav" style="none" />
				</div>			
			</div>
			<?php endif ?>				
			<div class="span12 footer">
				<jdoc:include type="modules" name="footer" style="none" />
			</div>		
		</div>		
	</div>
</footer>
<div id="copyright" class="fullwidth <?php print (!$this->countModules('bottom7') && $this->countModules('bottom1')) ? "siteEnd" : "else"; ?>">
	<div class="copyWrapper innerwidth">
		<p>&copy; <?php print date("Y"); ?> BEYER Rechtsanwaltsgesellschaft mbH
		<?php if( !$detect->isMobile() || $detect->isTablet() ) : ?> 
			<?php if($this->language == "de-de") : ?>
				 | <a class="imprLink" href="/de/impressum.html">Impressum</a> | <a class="imprLink" href="/de/datenschutz.html">Datenschutz</a></p>
			<?php else : ?>
				 | <a class="imprLink" href="/en/imprint.html">Imprint</a> | <a class="imprLink" href="/en/privacy-policy.html">Privacy Policy</a></p>
			<?php endif; ?>
		<?php endif; ?>
	</div>
</div>	
		