<?php
/**
 * @author   	089webdesgin.de
 * @copyright   Copyright (C) 2015 089webdesgin.de. All rights reserved.
 * @URL 		https://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
 
defined('_JEXEC') or die;
?>
	
<header id="header" class="fullwidth">
	<div class="innerwidth flex">
		<div class="row-fluid">        				
			<div class="span1 pull-right langIcons">
				<?php /*<nav>
					<a class="" href="/de/rechtsanwalt-kanzlei-beyer-muenchen.html" title="Deutsche Sprache wählen">
						<img class="langSelect" src="/images/de.gif" alt="Hier klicken für deutschsprachige Ausgabe" />
					</a>
					<a class="" href="/en/home-en.html" title="Choose english language">
						<img class="langSelect" src="/images/en.gif" alt="Click here for english content" />
					</a>				
				</nav> */
				?>
				<jdoc:include type="modules" name="langMod" style="custom" />
			</div>
			<div class="span3 pull-right">
				<div class="vcard clr">
					<span class="org">Dr. Beyer Rechtsanwaltsgesellschaft mbH</span>				  
					  <p class="tel "><a class="" href="tel:+49899972750">&nbsp;+49 - (0)89 99 72 75 - 0</a></p>
					  <p class="adr"><span class="street-address">Ismaninger Straße 102 - 106 </span>
					    <span class="postal-code"> D-81675 </span><span class="region">M&uuml;nchen</span>
					  </p>
				</div>
			</div>	          
		</div>
		<div class="orgHeader">
			<a class="brand" href="<?php echo $this->baseurl; ?>">
				<h2>Beyer <span>Rechtsanwaltsgesellschaft mbH</span></h2>	
			</a>
		</div>
		<div class="countryTags">
			<h4>M&uuml;nchen </h4><h4>Luanda </h4><h4>Sao Paulo </h4><h4>Buenos Aires </h4><h4>Lissabon </h4><h4>Athen </h4><h4>Zagreb </h4><h4>Ljubljana </h4><h4>Prag </h4>
			<h4>Bratislava </h4><h4>Kiew </h4><h4>Miami</h4>
		</div>	
	</div>
</header>